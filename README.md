# Sums of Powers-of-Two in Adaptive Filtering

*Sums of Powers-of-Two* (SOPOT) is a numeric representation in which a number is 
not represented only using 0's and 1's, but also using -1. This creates a more
sparse numeric representation and therefore reduces the computational complexity
of multiplication operations.


## SOPOT Class

Create a MATLAB class to operate all the SOPOT arithmetic overloading the usual 
operators:

___

1. Create constructor:
    The constructor method is used to define the objects, i.e., is the function
called in a script when the user wants to create an object. It has the same
name as the class (MatrixSOPOT).
* [x] public constructor
* [x] private constructor (to be used within class) *(can be unused)*

___

2. Overload operators: (can be accessed outside class)
* [x] operator (+) plus
* [x] operator (-) minus
* [x] operator (.*) times
* [x] operator (*) mtimes
* [ ] operator (./) rdivide
* [ ] operator (/) mrdivide
* [ ] operator (') transpose

___

3. Define public object's properties: (can be accessed outside class)
* [x] **Value** -- Approximated value
* [x] **Wordlength** -- Wordlength
* [x] **MaxNumSPT** -- Number of SPT terms
* [x] **MaxPot**  -- Highest power of two
* [x] **SOPOTMat** -- Tensor containing the SOPOT representation
* [x] **RealValue** -- Real Value

___

4. Define private object's properties: (can only be accessed from within class)
* [x] **RowNum** -- Number of rows
* [x] **ColNum** -- Number of columns

___

5. Define private methods: (can only be accessed from within class)
* [x] **reapprox()** -- Reapproximation (*static=true*) (Separate in two parts? common term reduction and overflow/roundoff)
* [x] **mpgbp()** -- MPGBP SOPOT approximation (*static=true*)
* [x] **recover()** -- Recover from SOPOT format (*static=true*)
* [x] **base_product()** -- Basic SOPOT product (*static=true*) (is it possible to don't use for loops?)


___
### Observations:
+ *static=true* means that the input does not have to be an object. Those can be 
called to operate on usual matlab variables.


## Related Work:
* da Silva, E.A.B., Lovisolo, L., Dutra, A.J.S. and Diniz, P.S.R., 2014. 
"FIR filter design based on successive approximation of vectors". 
*IEEE transactions on signal processing*, 62(15), pp.3833-3848.

* Coelho, L.F.S., Lovisolo, L., Tcheou, M.P., 2018. "Sobre o Desempenho de 
Filtros Adaptativos com Coeficientes em Somas de Potências de Dois em Realce de
Sinais". *XXXVI Simpósio Brasileiro de Telecomunicações e Processamento de Sinais*.
